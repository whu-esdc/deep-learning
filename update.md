

- 2021.04.26
  
  更新了手写数字代码的GPU训练版本，增加了训练模型的保存功能

  - basic_deepLearning/src/minist-torch-bp-gpu.py

  - basic_deepLearning/src/minist-torch-cnn-gpu.py